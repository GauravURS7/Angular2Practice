import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

import { ProdArray } from '../product/prodArray';

@Injectable()
export class ProductService {
 
    constructor( private http : Http){
        console.log('ProductService.....');
    }
    //-----these lines used for calling the product data from server (backend database)
    productUrl:string = "http://localhost:8085/ProductApp/productTypeList";
    productAttrUrl:string = "http://localhost:8085/ProductApp/searchProductDetailsByProductTypeId-";
    
    getProductTypeList(){
        return this.http.get(this.productUrl).map(response => response.json());
    }
    
    getProductAttributeList(id:number) {
        return this.http.get(this.productAttrUrl+id).map(response => response.json());
    }
    //-------end of using http ajax call
    
    
    //local crud application terms
    prod = [
        {"name" : "nokia", "price" : 20000},
        {"name" : "samsung", "price" : 30000},
        {"name" : "moto", "price" :40000},
        {"name" : "LG", "price" : 50000},
    ]

    getProducts() : any{
        return this.prod;
    }
    
    setProducts(name:string, price:number, id : number){
    
        if(id === null) {
            this.prod.push({
                name : name,
                price : price
            });
        }
        else {            
            this.prod[id] = {
                name : name,
                price : price
            }
        }
    }
    
    delProducts(pd:number) {
        this.prod.splice(pd,1);
    }
    //----local
}
