import { Component, Input } from '@angular/core';
import { ProductService } from '../service/product.service';
import { ProdArray } from './prodArray';

@Component({

    selector : 'product',
    templateUrl : './product.component.html',
    styleUrls : ['./product.component.css'],
    providers : [ ProductService ]
})

export class ProductComponent {
    
    constructor(public productService:ProductService){
        this.prod = this.productService.getProducts();
        
        this.productService.getProductTypeList().subscribe(product => {
         this.productType = product;
        });
        
    }
 
 //getting the data from server sides according to search
    public query = '';
    public filteredList = [];
    
    filter() {
        if (this.query !== ""){
            this.filteredList = this.productType.filter(function(el){
                //console.log(el);
                return el.productTypeName.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
                
            }.bind(this));
        }else{
            this.filteredList = [];
        }
    }
 
    select(item){
    console.log(item);
        this.query = item.productTypeDisplayName;
        this.filteredList = [];
        
        this.productService.getProductAttributeList(item.productTypeId).subscribe( productAttr => {
          this.productAttr = productAttr;
          console.log(this.productAttr);
        });
    }
    //----------end of using ajax call for getting daata from server
    
    //these lines simple crud application 
    objectIndex:number;
    pName:string;
    pPrice:number; 
    @Input() prod: ProdArray;
 
    editProd(i:number){
        this.objectIndex = i;
        this.pName = this.prod[i].name;
        this.pPrice = this.prod[i].price;
    }
    
    saveProd(){
        this.productService.setProducts(this.pName, this.pPrice,this.objectIndex);
        this.objectIndex = null;
    }
    
    delProd(pd:number){
       this.productService.delProducts(pd);
       //this.prod.splice(pd,1);
    }  

}
